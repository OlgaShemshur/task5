/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlvalidation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static xmlvalidation.FileName.RESULT;
import static xmlvalidation.FileName.XML_DATA;
import static xmlvalidation.FileName.XSD;
import static xmlvalidation.FileName.XSL;

/**
 *
 * @author Olga
 */
public class XMLvalidationTest {

    /**
     * Test of checkXMLforXSD method, of class XMLvalidation.
     */
    @Test
    public void testCheckXMLforXSD() throws Exception {
        System.out.println("checkXMLforXSD");
        String pathXml = XML_DATA;
        String pathXsd = XSD;
        boolean expResult = true;
        boolean result = XMLvalidation.checkXMLforXSD(pathXml, pathXsd);
        assertEquals(expResult, result);
    }

    /**
     * Test of transform method, of class XMLvalidation.
     */
    @Test
    public void testTransform() throws Exception {
        System.out.println("transform");
        boolean expResult = true;
        boolean result = XMLvalidation.transform(XML_DATA, XSL, RESULT);
        assertEquals(expResult, result);
    }
    
}
