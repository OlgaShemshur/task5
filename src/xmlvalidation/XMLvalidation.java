package xmlvalidation;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.xml.sax.SAXException;
import static xmlvalidation.FileName.*;
 
public class XMLvalidation {
     
    /**
     * @param pathXml - ���� � XML
     * @param pathXsd - ���� � XSD
     */
    static{
        new DOMConfigurator().doConfigure(LOG4G, LogManager.getLoggerRepository());
    }
    static Logger log = Logger.getLogger("log");
    
    public static boolean checkXMLforXSD(String pathXml, String pathXsd)
            throws Exception {
 
        try {
            File xml = new File(pathXml);
            File xsd = new File(pathXsd);
             
            if (!xml.exists()) {
                log.info("Do not found XML " + pathXml);
            }
             
            if (!xsd.exists()) {
                log.info("Do not found XSD " + pathXsd);
            }
             
            if (!xml.exists() || !xsd.exists()) {
                return false;
            }
 
            SchemaFactory factory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(pathXsd));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(pathXml));
            return true;
        } catch (SAXException e) {
            log.error(e.getMessage());
            return false;
        }
 
    }
 
    public static void main(String[] args) throws Exception {
        boolean b = XMLvalidation.checkXMLforXSD(XML_DATA, XSD);
        log.info("XML complies XSD : " + b);
        transform(XML_DATA, XSL, RESULT);
    }
    
    public static boolean transform(String pathXml, String pathXsl, String pathResult) throws IOException, URISyntaxException, TransformerException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xsl = new StreamSource(new File(pathXsl));
        Transformer transformer = factory.newTransformer(xsl);

        Source text = new StreamSource(new File(pathXml));
        try{
            transformer.transform(text, new StreamResult(new File(pathResult)));
            return true;
        }
        catch (Exception e){
            log.error(e);
            return false;
        }
    }
}