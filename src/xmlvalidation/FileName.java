/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlvalidation;

/**
 *
 * @author Olga
 */
public interface FileName {
    String LOG4G = "src\\resource\\log4j.xml";
    String XML_DATA = "src\\resource\\data.xml";
    String XSL = "src\\resource\\stylesheet.xsl";
    String XSD = "src\\resource\\schema.xsd";
    String RESULT = "src\\resource\\result.html";
}
